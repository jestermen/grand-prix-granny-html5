var com;
(function (com) {
    var jestermen;
    (function (jestermen) {
        var grandPrixGranny;
        (function (grandPrixGranny) {
            var Main = (function () {
                //=================================
                //		SETUP
                //=================================
                function Main() {
                    var _this = this;
					//CONSTANTS
                    this.GAME_INITED = 0;
                    this.GAME_RESET = 1;
                    this.GAME_PLAYING = 2;
                    this.GAME_OVER = 3;
                    this.GAME_PAUSED = 4;
                    this.GAME_DISORIENTED = 5;
                    this.DEFAULT_CANVAS_WIDTH = 800;
                    this.DEFAULT_CANVAS_HEIGHT = 450;
                    this.NUM_NPC_ROWS = 2;
                    this.NUM_NPCS_PER_ROW = 5;
                    this.NPC_ROW_START = -430; //the y-coordinate of the first row of NPCs
                    this.NPC_ROW_MARGIN = 430; //number of pixels between each row of NPCs
                    this.TRACK_LEFT = 120; //far left side of the actual race track on the background
                    this.TRACK_RIGHT = this.DEFAULT_CANVAS_WIDTH - 120;
                    this.TRACK_WIDTH = this.TRACK_RIGHT - this.TRACK_LEFT;
                    this.BG_SPEED = 2.5;
                    this._currentScore = 0;
                    this._highScore = 0;
                    this._gameState = 0;
                    this._prevGameState = 0;
                    this._currentEmptySlotIndex = 0;
                    this._inputX = 0;
                    this.HandleLoadedAssets = function () {
                        _this.ContinueInit();
                    };
                    this.GameLoop = function () {
                        if (_this._gameState == _this.GAME_PLAYING) {
                            _this.UpdateWorld();
                            _this.UpdatePC();
                            _this.UpdateNPCs();
                            _this.DetectCollisions();
                        }
                        else if (_this._gameState == _this.GAME_OVER) {
                            _this.UpdatePC();
                        }
                    };
                    this.UpdateDisplay = function () {
                        _this._uiManager.update(_this._currentScore, _this._highScore, _this._gameState == _this.GAME_PAUSED, _this._isMuted);
                        _this._stage.update();
                    };
                    this.HandleGameEnded = function () {
                        _this.StartResetState();
                    };
                    console.log("Grand Prix Granny v0.0.1");
                    this.StartInitState();
                }
                //=================================
                //		STATE MANAGEMENT
                //=================================
                /**
                * Starts the initialization state, which the game will start in. In this state, all initially relevant assets are loaded.
                *
                * FROM: None. Default state.
                * TO: Reset State, when all initially relevant assets are loaded.
                */
                Main.prototype.StartInitState = function () {
                    console.log("Main.StartInitState()");
                    this._prevGameState = this._gameState = this.GAME_INITED;
                    this.LoadAssets();
                };
                Main.prototype.LoadAssets = function () {
                    grandPrixGranny.AssetManager.loadAssets(this.HandleLoadedAssets);
                };
                Main.prototype.ContinueInit = function () {
                    //Determine OS so we know how big to make the canvas element
                    if (new MobileDetect(window.navigator.userAgent).mobile()) {
                        this._isMobile = true;
                        this._canvasWidth = window.outerWidth * window.devicePixelRatio;
                        this._canvasHeight = window.outerHeight * window.devicePixelRatio;
                    }
                    else {
                        this._isMobile = false;
                        this._canvasWidth = this.DEFAULT_CANVAS_WIDTH;
                        this._canvasHeight = this.DEFAULT_CANVAS_HEIGHT;
                    }
                    var canvas = document.getElementById("mainCanvas");
                    canvas.style.background = "#3498DB";
                    canvas.setAttribute("width", this._canvasWidth.toString());
                    canvas.setAttribute("height", this._canvasHeight.toString());
                    //Create the stage based on the updated canvas
                    this._stage = new createjs.Stage("mainCanvas");
                    this._stage.enableMouseOver();
                    createjs.Touch.enable(this._stage);
                    //Create the object that will contain all game elements
                    this._worldContainer = new createjs.Container();
                    this._worldContainer.scaleX = this._worldContainer.scaleY = this._canvasHeight / this.DEFAULT_CANVAS_HEIGHT;
                    this._worldContainer.x = (this._canvasWidth - (this.DEFAULT_CANVAS_WIDTH * this._worldContainer.scaleX)) / 2;
                    this._stage.addChild(this._worldContainer);
                    //Create the backgrounds
                    this._bg1 = new createjs.Bitmap(grandPrixGranny.AssetManager.getAssetByID("bg"));
                    this._worldContainer.addChild(this._bg1);
                    this._bg2 = new createjs.Bitmap(grandPrixGranny.AssetManager.getAssetByID("bg"));
                    this._worldContainer.addChild(this._bg2);
                    //Create the NPCs
                    this._npcs = [];
                    var newNPC;
                    for (var i = 0; i < this.NUM_NPC_ROWS; i++) {
                        var row = [];
                        for (var j = 0; j < this.NUM_NPCS_PER_ROW; j++) {
                            newNPC = new grandPrixGranny.NPC();
                            this._worldContainer.addChild(newNPC.view);
                            row[j] = newNPC;
                        }
                        this._npcs[i] = row;
                    }
                    //Create the PC
                    this._pc = new grandPrixGranny.PC();
                    this._worldContainer.addChild(this._pc.view);
                    //Retrieve locally stored pref: mute
                    this._isMuted = this.GetCookie("bGPGMuted") == "1";
                    if (this._isMuted) {
                        this.MuteGame();
                    }
                    else {
                        this.UnmuteGame();
                    }
                    //Create the sound effects
                    this._crash = createjs.Sound.createInstance("car_crash");
                    this._music = createjs.Sound.createInstance("music");
                    this._music.loop = -1;
                    //Retrieve locally stored high score
                    this._highScore = parseInt(this.GetCookie("iGPGHighScore"));
                    if (isNaN(this._highScore)) {
                        console.log("Main.BuildWorld(): No high score or invalid high score retrieved. Setting high score to 0!");
                        this._highScore = 0;
                        this.SetCookie("iGPGHighScore", "0");
                    }
                    //Initialize the game loop
                    createjs.Ticker.framerate = 60;
                    //Make sure stage is always visible (this is not the game loop)
                    createjs.Ticker.addEventListener("tick", this.UpdateDisplay);
                    //UI
                    this._uiManager = new grandPrixGranny.UIManager();
                    this._uiManager.startInitState(this, this._worldContainer, this._canvasWidth, this._canvasHeight, this._isMobile);
                    this._stage.addChild(this._uiManager.guiContainer);
                    //Reset all game objects to their initial positions
                    this.StartResetState();
                };
                /**
                * Starts the reset state. In this state, the score and all game objects are returned to their starting points and the player is prompted to start the game.
                *
                * FROM: Init State, when all initially relevant assets are loaded.
                * FROM: Game Over State, when the player loses the game.
                * TO: Paused State, when all game objects are returned to their starting points.
                */
                Main.prototype.StartResetState = function () {
                    console.log("Main.StartResetState()");
                    this._prevGameState = this._gameState;
                    this._gameState = this.GAME_RESET;
                    //Reset game objects
                    this._currentScore = 0;
                    this._bg1.y = 0;
                    this._bg2.y = -this._bg1.getBounds().height;
                    this._pc.reset();
                    for (var i = 0; i < this._npcs.length; i++) {
                        this.ResetNPCRow(this._npcs[i]);
                        //The first row's y-coordinate is set manually. Subsequent rows will fall in behind it
                        if (i == 0) {
                            for (var j = 0; j < this._npcs[i].length; j++) {
                                this._npcs[i][j].view.y = this.NPC_ROW_START;
                            }
                        }
                    }
                    //UI
                    this._uiManager.startResetState();
                    this.StartPausedState();
                };
                Main.prototype.ResetNPCRow = function (row) {
                    //Randomly place an empty slot amidst the cars in this row
                    var numSlots = this.NUM_NPCS_PER_ROW + 1; //+1 to account for the empty slot
                    var emptySlotIndex;
                    do {
                        emptySlotIndex = Math.floor(Math.random() * numSlots);
                    } while (emptySlotIndex == this._currentEmptySlotIndex);
                    this._currentEmptySlotIndex = emptySlotIndex;
                    //Update coordinates
                    var thisNPC;
                    for (var i = 0; i < numSlots; i++) {
                        if (i == emptySlotIndex) {
                            continue;
                        }
                        else {
                            if (i > emptySlotIndex) {
                                thisNPC = row[i - 1];
                            }
                            else {
                                thisNPC = row[i];
                            }
                            var marginWidth = (this.TRACK_WIDTH - (numSlots * thisNPC.view.getBounds().width)) / (numSlots + 1);
                            var segmentWidth = marginWidth + thisNPC.view.getBounds().width;
                            var thisNPCX = this.TRACK_LEFT + segmentWidth * i + marginWidth;
                            thisNPC.reset();
                            thisNPC.view.x = thisNPCX;
                        }
                    }
                    //Update y-coordinate: find the row in front of this one, then put this row above that row on the screen
                    var thisRowIndex = this._npcs.indexOf(row);
                    var nextRowIndex = (thisRowIndex == 0) ? this._npcs.length - 1 : thisRowIndex - 1;
                    for (i = 0; i < row.length; i++) {
                        thisNPC = row[i];
                        if (this._npcs.length == 1) {
                            thisNPC.view.y = -this.NPC_ROW_MARGIN;
                        }
                        else {
                            thisNPC.view.y = this._npcs[nextRowIndex][0].view.y - this.NPC_ROW_MARGIN;
                        }
                    }
                };
                /**
                * Starts the paused state. In this state, the game loop stops and the player has no input options except to unpause the game. If the device is in portrait mode upon entering the paused
                * state, the game will immediately switch to disoriented state.
                *
                * FROM: Reset State, when all game objects are returned to their starting points.
                * FROM: Playing State, when the player pauses the game.
                * FROM: Disoriented State, when the player turns the mobile device to landscape mode.
                * TO: Playing State, when the player unpauses the game.
                * TO: Disoriented State, if the mobile device is in portrait mode.
                */
                Main.prototype.StartPausedState = function () {
                    console.log("Main.StartPausedState()");
                    this._prevGameState = this._gameState;
                    this._gameState = this.GAME_PAUSED;
                    this.StopGameLoop();
                    //Pause music
                    this._music.paused = true;
                    //Clear user input
                    this._inputX = 0;
                    //UI
                    this._uiManager.startPausedState();
                    //If we're on mobile and in portrait mode, switch to disoriented state
                    if (this._isMobile && !this.IsDeviceLandscape()) {
                        this.StartDisorientedState();
                    }
                };
                /**
                * Starts the disoriented state. In this state, the game loop stops and the player has no input options except to turn the mobile device to landscape mode.
                *
                * FROM: Paused State, if the mobile device is in portrait mode.
                * TO: Paused State, when the player turns the mobile device to landscape mode.
                */
                Main.prototype.StartDisorientedState = function () {
                    //console.log("Main.StartDisorientedState()");
                    //UI
                    this._uiManager.startDisorientedState();
                };
                /**
                * Starts the playing state. In this state, the player controls the PC and plays the game.
                *
                * FROM: Reset State, when the player starts the game.
                * FROM: Paused State, when the player unpauses the game.
                * TO: Paused State, when the player pauses the game or turns the mobile device to portrait mode.
                * TO: Game Over State, when the player loses the game.
                */
                Main.prototype.StartPlayingState = function () {
                    //console.log("Main.StartPlayingState()");
                    this._prevGameState = this._gameState;
                    this._gameState = this.GAME_PLAYING;
                    //Turn on music
                    this._music.paused = false;
                    this._music.play();
                    //If on mobile, try to go full screen
                    if (this._isMobile)
                        this.RequestFullscreen();
                    this.StartGameLoop();
                    //UI
                    this._uiManager.startPlayingState();
                };
                Main.prototype.RequestFullscreen = function () {
                    var canvas = document.getElementById("mainCanvas");
                    if (canvas.requestFullscreen) {
                        canvas.requestFullscreen();
                    }
                    else if (canvas.webkitRequestFullscreen) {
                        canvas.webkitRequestFullscreen();
                    }
                    else {
                        console.log("Main.RequestFullscreen(): Fullscreen not supported.");
                    }
                };
                Main.prototype.StartGameLoop = function () {
                    createjs.Ticker.addEventListener("tick", this.GameLoop);
                };
                Main.prototype.StopGameLoop = function () {
                    createjs.Ticker.removeEventListener("tick", this.GameLoop);
                };
                Main.prototype.UpdateWorld = function () {
                    this._bg1.y += this.BG_SPEED;
                    this._bg2.y += this.BG_SPEED;
                    //Flip flop BG
                    if (this._bg1.y > this.DEFAULT_CANVAS_HEIGHT) {
                        this._bg1.y = this._bg2.y - this._bg1.getBounds().height;
                    }
                    else if (this._bg2.y > this.DEFAULT_CANVAS_HEIGHT) {
                        this._bg2.y = this._bg1.y - this._bg2.getBounds().height;
                    }
                };
                Main.prototype.UpdatePC = function () {
                    this._pc.update(this._inputX);
                };
                Main.prototype.UpdateNPCs = function () {
                    for (var i = 0; i < this._npcs.length; i++) {
                        for (var j = 0; j < this._npcs[i].length; j++) {
                            this._npcs[i][j].update();
                        }
                    }
                };
                Main.prototype.DetectCollisions = function () {
                    //PC VS. WALLS
                    var pcLeft = this._pc.view.x;
                    var pcRight = pcLeft + this._pc.view.getBounds().width;
                    if (pcLeft < this.TRACK_LEFT || pcRight > this.TRACK_RIGHT) {
                        this._pc.view.x -= this._pc.dx;
                        this._pc.dx = -this._pc.dx * .9;
                    }
                    //PC VS. NPCS
                    var thisNPC;
                    pcVnpc: for (var i = 0; i < this._npcs.length; i++) {
                        for (var j = 0; j < this._npcs[i].length; j++) {
                            thisNPC = this._npcs[i][j];
                            if (this.IsColliding(this._pc, thisNPC)) {
                                this._crash.play();
                                this._pc.kill();
                                this.StartGameOverState();
                                break pcVnpc;
                            }
                        }
                    }
                    //NPCS VS. LEVEL
                    npcVlevel: for (var i = 0; i < this._npcs.length; i++) {
                        for (var j = 0; j < this._npcs[i].length; j++) {
                            thisNPC = this._npcs[i][j];
                            if (thisNPC.collisionBox.y > this.DEFAULT_CANVAS_HEIGHT + 30) {
                                this.ResetNPCRow(this._npcs[i]);
                                this._currentScore++;
                                break npcVlevel;
                            }
                        }
                    }
                };
                Main.prototype.IsColliding = function (char1, char2) {
                    if (char1.collisionBox.x > char2.collisionBox.x + char2.collisionBox.width || char1.collisionBox.x + char1.collisionBox.width < char2.collisionBox.x) {
                        return false;
                    }
                    if (char1.collisionBox.y > char2.collisionBox.y + char2.collisionBox.height || char1.collisionBox.y + char1.collisionBox.height < char2.collisionBox.y) {
                        return false;
                    }
                    return true;
                };
                /**
                * Starts the game over state. In this state, the player has no input options until the death animation is complete, after which the game will enter the reset state.
                *
                * FROM: Playing state, when the player loses the game.
                * TO: Reset State, when the PC death animation is complete.
                */
                Main.prototype.StartGameOverState = function () {
                    //console.log("Main.StartGameOverState()");
                    this._prevGameState = this._gameState;
                    this._gameState = this.GAME_OVER;
                    //Update high score
                    if (this._currentScore > this._highScore) {
                        this._highScore = this._currentScore;
                        this.SetCookie("iGPGHighScore", this._highScore.toString());
                    }
                    //Turn off music
                    this._music.paused = false;
                    this._music.stop();
                    //Clear user input
                    this._inputX = 0;
                    //Wait for the PC's death to finish animating
                    setTimeout(this.HandleGameEnded, 1500);
                    //UI
                    this._uiManager.startGameOverState();
                };
                //=================================
                //		UI MANAGEMENT
                //=================================
                //======	Device Orientation
                Main.prototype.handleUserChangedOrientation = function () {
                    this.StartPausedState();
                };
                //======	Mouse > World
                Main.prototype.handleUserMousedDownWorld = function (localMouseX) {
					if (localMouseX < this._pc.view.x + 40) {
                        this._inputX = -1;
                    }
                    else {
                        this._inputX = 1;
                    }
                };
                Main.prototype.handleUserMousedUpWorld = function () {
                    this._inputX = 0;
                };
                Main.prototype.HandleUserClickedWorld = function () {
                    this.StartPlayingState();
                };
                //======	Mouse > Pause Button
                /**
                * Toggles game pause.
                *
                * @return Returns true if the game is now paused, false if not.
                */
                Main.prototype.handleUserClickedPauseButton = function () {
                    //Toggle pause
                    if (this._gameState == this.GAME_PAUSED) {
                        this.StartPlayingState();
                        return false;
                    }
                    else {
                        this.StartPausedState();
                        return true;
                    }
                };
                //======	Mouse > Mute Button
                /**
                * Toggles game mute.
                *
                * @return Returns true if the game is now muted, false if not.
                */
                Main.prototype.handleUserClickedMuteButton = function () {
                    //Toggle and save mute state
                    if (this._isMuted) {
                        this.UnmuteGame();
                        return false;
                    }
                    else {
                        this.MuteGame();
                        return true;
                    }
                };
                //=================================
                //		MISC.
                //=================================
                Main.prototype.IsDeviceLandscape = function () {
                    switch (window.orientation) {
                        //Landscape
                        case -90:
                        case 90:
                            return true;
                        //Portrait
                        default:
                            return false;
                    }
                };
                Main.prototype.MuteGame = function () {
                    this._isMuted = true;
                    createjs.Sound.setMute(true);
                    this.SetCookie("bGPGMuted", "1");
                };
                Main.prototype.UnmuteGame = function () {
                    this._isMuted = false;
                    createjs.Sound.setMute(false);
                    this.SetCookie("bGPGMuted", "0");
                };
                //=================================
                //		COOKIES
                //=================================
                Main.prototype.GetCookie = function (cookieName) {
                    //console.log("Main.GetCookie(): document.cookie = " + document.cookie);
                    var name = cookieName + "=";
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var cookie = ca[i];
                        while (cookie.charAt(0) == " ")
                            cookie = cookie.substring(1);
                        if (cookie.indexOf(name) == 0)
                            return cookie.substring(name.length, cookie.length);
                    }
                    return "";
                };
                Main.prototype.SetCookie = function (cookieName, cookieValue) {
                    document.cookie = cookieName + "=" + cookieValue + "; expires=Fri, 31 Dec 9999 23:59:59 GMT";
                    //console.log("Main.SetCookie(): document.cookie = " + document.cookie);
                };
                return Main;
            })();
            grandPrixGranny.Main = Main;
        })(grandPrixGranny = jestermen.grandPrixGranny || (jestermen.grandPrixGranny = {}));
    })(jestermen = com.jestermen || (com.jestermen = {}));
})(com || (com = {}));
//# sourceMappingURL=Main.js.map