var com;
(function (com) {
    var jestermen;
    (function (jestermen) {
        var grandPrixGranny;
        (function (grandPrixGranny) {
            var AssetManager = (function () {
                function AssetManager() {
                }
                //=================================
                //		LOAD
                //=================================
                AssetManager.loadAssets = function (completeCallback) {
                    AssetManager._completeCallback = completeCallback;
                    var preload = new createjs.LoadQueue();
                    preload.addEventListener("fileload", this.HandleLoadedAsset);
                    preload.addEventListener("complete", this.HandleLoadedAssetsComplete);
                    preload.loadManifest([{ id: "bg", src: "assets/bg.jpg" },
                        { id: "mute_button", src: "assets/mute_button.png" },
                        { id: "pause_button", src: "assets/pause_button.png" },
                        { id: "npc", src: "assets/npc.png" },
                        { id: "pc", src: "assets/pc.png" },
                        { id: "pc_dead", src: "assets/pc_dead.png" }
                    ]);
                    createjs.Sound.registerSound("assets/car_crash.mp3", "car_crash");
                    createjs.Sound.registerSound("assets/music.mp3", "music");
                };
                //=================================
                //		ACCESSORS
                //=================================
                AssetManager.getAssetByID = function (id) {
                    return AssetManager._assets[id];
                };
                AssetManager._assets = {};
                AssetManager.HandleLoadedAsset = function (event) {
                    AssetManager._assets[event.item.id] = event.result;
                };
                AssetManager.HandleLoadedAssetsComplete = function (event) {
                    AssetManager._completeCallback();
                };
                return AssetManager;
            })();
            grandPrixGranny.AssetManager = AssetManager;
        })(grandPrixGranny = jestermen.grandPrixGranny || (jestermen.grandPrixGranny = {}));
    })(jestermen = com.jestermen || (com.jestermen = {}));
})(com || (com = {}));
//# sourceMappingURL=AssetManager.js.map