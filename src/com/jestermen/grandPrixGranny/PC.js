var com;
(function (com) {
    var jestermen;
    (function (jestermen) {
        var grandPrixGranny;
        (function (grandPrixGranny) {
            var PC = (function () {
                //=================================
                //		SETUP
                //=================================
                function PC() {
                    //CONSTANTS
                    this.START_X = 400;
                    this.START_Y = 550;
                    this.END_Y = 450 - 32;
                    this.MAX_DX = 8; //pixels per frame
                    this.ACCELERATION_X = .5;
                    this.ACCELERATION_Y = -2.5;
                    this.DEAD_START_Y = -500;
                    this.DEAD_START_DY_MIN = -11;
                    this.DEAD_START_DY_MAX = -12;
                    this.DEAD_DECAY = .05; //how quickly the dead PC decelerates (percent per frame)
                    //PRIMITIVES
                    this._dx = 0;
                    this._dy = 0;
                    this._deadDX = 0;
                    this._deadDY = 0;
                    this.CreateViews();
                    this._collisionBox = new createjs.Rectangle(0, 0, 60, 80); //position updated later
                }
                PC.prototype.CreateViews = function () {
                    var viewSpriteSheetData = {
                        images: [grandPrixGranny.AssetManager.getAssetByID("pc")],
                        frames: { width: 80, height: 110 }
                    };
                    var viewSpriteSheet = new createjs.SpriteSheet(viewSpriteSheetData);
                    this._view = new createjs.Sprite(viewSpriteSheet);
                    this._view.gotoAndStop(0);
                    var viewDeadSpriteSheetData = {
                        images: [grandPrixGranny.AssetManager.getAssetByID("pc_dead")],
                        frames: { width: 66, height: 96 }
                    };
                    var viewDeadSpriteSheet = new createjs.SpriteSheet(viewDeadSpriteSheetData);
                    this._viewDead = new createjs.Sprite(viewDeadSpriteSheet);
                    this._viewDead.regX = this._viewDead.getBounds().width / 2;
                    this._viewDead.regY = this._viewDead.getBounds().height / 2;
                    this._viewDead.visible = false;
                };
                PC.prototype.reset = function () {
                    this._dx = 0;
                    this._dy = 0;
                    this._deadDX = 0;
                    this._deadDY = 0;
                    this._isVeering = false;
                    this._isDead = false;
                    this._view.gotoAndStop(0);
                    this._view.x = this.START_X - this._view.getBounds().width / 2;
                    this._view.y = this.START_Y;
                    this._viewDead.visible = false;
                };
                PC.prototype.update = function (inputX) {
                    if (this._isDead) {
                        this._viewDead.x += this._deadDX;
                        this._viewDead.y += this._deadDY;
                        //Decelerate
                        this._deadDX *= (1 - this.DEAD_DECAY);
                        this._deadDY *= (1 - this.DEAD_DECAY);
                        if (this._deadDY > -1) {
                            this._deadDX = 0;
                            this._deadDY = 0;
                        }
                        else {
                            this._viewDead.rotation += this._deadDY;
                        }
                    }
                    else {
                        //UPDATE DX
                        //If the player is touching the screen, accelerate PC toward the touch
                        if (inputX != 0) {
                            //Stop veering if needed
                            if (this._isVeering) {
                                this._isVeering = false;
                                this._dx = 0; //so the player doesn't have to fight against the inertia
                            }
                            //If player is pressing left, accelerate left
                            if (inputX < 0) {
                                this._dx -= this.ACCELERATION_X;
                            }
                            else if (inputX > 0) {
                                this._dx += this.ACCELERATION_X;
                            }
                        }
                        else if (!this._isVeering) {
                            if (this._dx < 0) {
                                this._dx += this.ACCELERATION_X;
                                if (this._dx > 0)
                                    this._dx = 0;
                            }
                            else if (this._dx > 0) {
                                this._dx -= this.ACCELERATION_X;
                                if (this._dx < 0)
                                    this._dx = 0;
                            }
                        }
                        //Bounds check dx
                        if (this._dx < -this.MAX_DX) {
                            this._dx = -this.MAX_DX;
                        }
                        else if (this._dx > this.MAX_DX) {
                            this._dx = this.MAX_DX;
                        }
                        else if (this._dx == 0) {
                            this._isVeering = true;
                            //Randomly make the PC veer left or right
                            if (Math.random() < .5) {
                                this._dx = -this.ACCELERATION_X;
                            }
                            else {
                                this._dx = this.ACCELERATION_X;
                            }
                        }
                        //UPDATE DY
                        //If PC has reached the ending y-coordinate, no need to move it
                        if (this._view.y == this.END_Y - this._view.getBounds().height) {
                            this._dy = 0;
                        }
                        else {
                            this._dy = this.ACCELERATION_Y;
                        }
                        //UPDATE VIEW
                        this._view.x += this._dx;
                        this._view.y += this._dy;
                        //BOUNDS CHECK Y
                        if (this._view.y < this.END_Y - this._view.getBounds().height) {
                            this._view.y = this.END_Y - this._view.getBounds().height;
                        }
                        //UPDATE COLLISION BOX
                        this._collisionBox.x = this._view.x + 10;
                        this._collisionBox.y = this._view.y + 15;
                    }
                };
                PC.prototype.kill = function () {
                    this._isDead = true;
                    this._view.gotoAndStop(1);
                    //Center dead PC on PC
                    this._viewDead.x = this._view.x + this._view.getBounds().width / 2;
                    this._viewDead.y = this._view.y + this._view.getBounds().height / 2;
                    this._viewDead.visible = true;
                    this._view.parent.addChild(this._viewDead);
                    //Randomly determine how quickly the dead PC will start flying forward
                    var randDY = Math.random() * (this.DEAD_START_DY_MAX - this.DEAD_START_DY_MIN) + this.DEAD_START_DY_MIN;
                    this._deadDX = this._dx;
                    this._deadDY = randDY;
                    //Randomly set start rotation
                    var randAngle = Math.random() * 180;
                    if (Math.random() < .5)
                        randAngle *= -1;
                    this._viewDead.rotation = randAngle;
                };
                Object.defineProperty(PC.prototype, "collisionBox", {
                    //=================================
                    //		ACCESSORS
                    //=================================
                    get: function () {
                        return this._collisionBox;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(PC.prototype, "dx", {
                    get: function () {
                        return this._dx;
                    },
                    set: function (value) {
                        this._dx = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(PC.prototype, "dy", {
                    set: function (value) {
                        this._dy = value;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(PC.prototype, "view", {
                    get: function () {
                        return this._view;
                    },
                    enumerable: true,
                    configurable: true
                });
                return PC;
            })();
            grandPrixGranny.PC = PC;
        })(grandPrixGranny = jestermen.grandPrixGranny || (jestermen.grandPrixGranny = {}));
    })(jestermen = com.jestermen || (com.jestermen = {}));
})(com || (com = {}));
//# sourceMappingURL=PC.js.map