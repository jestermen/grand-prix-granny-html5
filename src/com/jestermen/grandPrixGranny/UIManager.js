var com;
(function (com) {
    var jestermen;
    (function (jestermen) {
        var grandPrixGranny;
        (function (grandPrixGranny) {
            var UIManager = (function () {
                //=================================
                //		SETUP
                //=================================
                function UIManager() {
                    var _this = this;
                    //=================================
                    //		USER INPUT
                    //=================================
                    //======	Device Orientation
                    this.HandleUserChangedOrientation = function () {
                        _this._main.handleUserChangedOrientation();
                    };
                    //======	Mouse > World
                    this.HandleUserMousedDownWorld = function (event) {
                        _this._main.handleUserMousedDownWorld(event.localX);
                    };
                    this.HandleUserMousedUpWorld = function (event) {
                        _this._main.handleUserMousedUpWorld();
                    };
                    this.HandleUserClickedWorld = function (event) {
                        _this._main.HandleUserClickedWorld();
                    };
                    //======	Mouse > Pause Button
                    this.HandleUserMousedOutPauseButton = function (event) {
                        _this._pauseButton.scaleX = _this._pauseButton.scaleY = 1;
                    };
                    this.HandleUserMousedDownPauseButton = function (event) {
                        _this._pauseButton.scaleX = _this._pauseButton.scaleY = .8;
                    };
                    this.HandleUserClickedPauseButton = function (event) {
                        _this._pauseButton.scaleX = _this._pauseButton.scaleY = 1;
                        _this._main.handleUserClickedPauseButton();
                    };
                    //======	Mouse > Mute Button
                    this.HandleUserMousedOutMuteButton = function (event) {
                        _this._muteButton.scaleX = _this._muteButton.scaleY = 1;
                    };
                    this.HandleUserMousedDownMuteButton = function (event) {
                        _this._muteButton.scaleX = _this._muteButton.scaleY = .8;
                    };
                    this.HandleUserClickedMuteButton = function (event) {
                        _this._muteButton.scaleX = _this._muteButton.scaleY = 1;
                        _this._main.handleUserClickedMuteButton();
                    };
                }
                /**
                * Notifies the UI of all relevant, up-to-date information to show the user.
                */
                UIManager.prototype.update = function (currentScore, highScore, isPaused, isMuted) {
                    //Current score text field
                    this._txtCurrentScore.text = currentScore.toString();
                    //High score text field
                    this._txtHighScore.text = "High Score: " + highScore;
                    //Pause button
                    this._pauseButton.gotoAndStop(isPaused ? 0 : 1);
                    //Mute button
                    this._muteButton.gotoAndStop(isMuted ? 0 : 1);
                };
                //=================================
                //		STATE MANAGEMENT
                //=================================
                /**
                * Starts the initialization state, which the game will start in. In this state, all UI elements are created.
                *
                * FROM: None. Default state.
                * TO: Reset State, when all initially relevant assets are loaded.
                *
                * @param main A reference to the main game object. Required in order to notify it of user interaction. This reference would be replaced with Observer Manager in a larger project.
                * @param worldContainer A reference to the object that contains the game objects. Required in order to add user input listeners.
                * @param visibleWidth The width of the screen area visible to the user.
                * @param visibleHeight The height of the screen area visible to the user.
                * @param isMobile Whether we're on a mobile device.
                */
                UIManager.prototype.startInitState = function (main, worldContainer, visibleWidth, visibleHeight, isMobile) {
                    this._main = main;
                    this._worldContainer = worldContainer;
                    //Create the parent object and the bounds that will be used for the UI elements
                    this._guiContainer = new createjs.Container();
                    this._guiBounds = new createjs.Rectangle(0, 0, visibleWidth, visibleHeight);
                    //Create the shadow that will be applied to the UI text fields
                    var textShadow = new createjs.Shadow("#000000", 2, 2, 3);
                    //Current score text field
                    this._txtCurrentScore = new createjs.Text("0", "36px Geo", "#FFFFFF");
                    this._txtCurrentScore.textAlign = "right";
                    this._txtCurrentScore.shadow = textShadow;
                    this._guiContainer.addChild(this._txtCurrentScore);
                    //High score text field
                    this._txtHighScore = new createjs.Text("High Score: 0", "60px Geo", "#FFFFFF");
                    this._txtHighScore.shadow = textShadow;
                    this._guiContainer.addChild(this._txtHighScore);
                    //Start prompt text field
                    this._txtStartPrompt = new createjs.Text((isMobile) ? "Tap to Start" : "Click to Start", "48px Geo", "#FFFFFF");
                    this._txtStartPrompt.shadow = textShadow;
                    this._guiContainer.addChild(this._txtStartPrompt);
                    //Orientation prompt text field
                    this._txtOrientationPrompt = new createjs.Text("To continue playing, \n turn your device \n to landscape orientation.", "36px Geo", "#FFFFFF");
                    this._txtOrientationPrompt.textAlign = "center";
                    this._txtOrientationPrompt.shadow = textShadow;
                    this._guiContainer.addChild(this._txtOrientationPrompt);
                    //Pause button
                    var pauseButtonSpritesheetData = {
                        images: [grandPrixGranny.AssetManager.getAssetByID("pause_button")],
                        frames: { width: 64, height: 64 }
                    };
                    var pauseButtonSpriteSheet = new createjs.SpriteSheet(pauseButtonSpritesheetData);
                    this._pauseButton = new createjs.Sprite(pauseButtonSpriteSheet);
                    this._pauseButton.regX = this._pauseButton.getBounds().width / 2;
                    this._pauseButton.regY = this._pauseButton.getBounds().height / 2;
                    this._pauseButton.cursor = "pointer";
                    this._pauseButton.addEventListener("mouseout", this.HandleUserMousedOutPauseButton);
                    this._pauseButton.addEventListener("mousedown", this.HandleUserMousedDownPauseButton);
                    this._guiContainer.addChild(this._pauseButton);
                    //Mute button
                    var muteButtonSpritesheetData = {
                        images: [grandPrixGranny.AssetManager.getAssetByID("mute_button")],
                        frames: { width: 64, height: 64 }
                    };
                    var muteButtonSpriteSheet = new createjs.SpriteSheet(muteButtonSpritesheetData);
                    this._muteButton = new createjs.Sprite(muteButtonSpriteSheet);
                    this._muteButton.regX = this._muteButton.getBounds().width / 2;
                    this._muteButton.regY = this._muteButton.getBounds().height / 2;
                    this._muteButton.cursor = "pointer";
                    this._muteButton.addEventListener("mouseout", this.HandleUserMousedOutMuteButton);
                    this._muteButton.addEventListener("mousedown", this.HandleUserMousedDownMuteButton);
                    this._guiContainer.addChild(this._muteButton);
                    //DEBUG
                    /*this._txtDebug = new createjs.Text("", "24px Geo", "#FFFFFF");
                    this._txtDebug.shadow = textShadow;
                    this._txtDebug.textAlign = "center";
                    this._txtDebug.text = "testing lots of things";
                    this._guiContainer.addChild(this._txtDebug);
                    this._txtDebug.text = "window.outerWidth/Height: " + window.outerWidth + "x" + window.outerHeight + "\n";
                    this._txtDebug.text += "window.innerWidth/Height: " + window.innerWidth + "x" + window.innerHeight + "\n";
                    this._txtDebug.text += "window.screen.width/height: " + window.screen.width + "x" + window.screen.height + "\n";
                    this._txtDebug.text += "window.screen.availWidth/Height: " + window.screen.availWidth + "x" + window.screen.availHeight + "\n";
                    this._txtDebug.text += "this._uiBounds.width/Height: " + this._uiBounds.width + "x" + this._uiBounds.height + "\n";
                    this._txtDebug.text += "DPR: " + devicePixelRatio;*/
                };
                /**
                * Starts the reset state. In this state, all UI elements and user input listeners are set to their default states.
                *
                * FROM: Init State, when all initially relevant assets are loaded.
                * FROM: Game Over State, when the player loses the game.
                * TO: Paused State, when all game objects are returned to their starting points.
                */
                UIManager.prototype.startResetState = function () {
                    //Current score text field
                    this._txtCurrentScore.x = this._guiBounds.width - 20;
                    this._txtCurrentScore.y = 5;
                    this._txtCurrentScore.visible = false;
                    //High score text field
                    this._txtHighScore.x = this._guiBounds.width / 2 - this._txtHighScore.getMeasuredWidth() / 2; //center
                    this._txtHighScore.y = this._guiBounds.height / 2 - this._txtHighScore.getMeasuredHeight() / 2 - 50; //just above center
                    this._txtHighScore.visible = false;
                    //Start prompt text field
                    this._txtStartPrompt.x = this._guiBounds.width / 2 - this._txtStartPrompt.getMeasuredWidth() / 2;
                    this._txtStartPrompt.y = this._guiBounds.height / 2 - this._txtStartPrompt.getMeasuredHeight() / 2 + 50;
                    this._txtStartPrompt.visible = false;
                    //Orientation prompt text field
                    this._txtOrientationPrompt.x = this._guiBounds.width / 3;
                    this._txtOrientationPrompt.y = this._guiBounds.height / 2 - this._txtOrientationPrompt.getMeasuredHeight() / 2 + 50;
                    this._txtOrientationPrompt.rotation = window.orientation;
                    this._txtOrientationPrompt.visible = false;
                    //Pause button
                    this._pauseButton.x = this._pauseButton.getBounds().width / 2 + 10;
                    this._pauseButton.y = this._pauseButton.getBounds().height / 2 + 10;
                    this._pauseButton.removeEventListener("click", this.HandleUserClickedPauseButton);
                    this._pauseButton.visible = false;
                    //Mute button
                    this._muteButton.x = this._pauseButton.x;
                    this._muteButton.y = this._pauseButton.y + this._pauseButton.getBounds().height / 2 + 10 + this._muteButton.getBounds().height / 2 + 10;
                    this._muteButton.removeEventListener("click", this.HandleUserClickedMuteButton);
                    this._muteButton.visible = false;
                    //Orientation changes
                    window.removeEventListener("orientationchange", this.HandleUserChangedOrientation);
                    //PC controls
                    this._worldContainer.removeEventListener("mousedown", this.HandleUserMousedDownWorld);
                    this._worldContainer.removeEventListener("pressup", this.HandleUserMousedUpWorld);
                    //World pause toggle
                    this._worldContainer.removeEventListener("click", this.HandleUserClickedWorld);
                };
                /**
                * Starts the paused state. In this state, only the appropriate UI elements are shown.
                *
                * FROM: Reset State, when all game objects are returned to their starting points.
                * FROM: Playing State, when the player pauses the game.
                * FROM: Disoriented State, when the player turns the mobile device to landscape mode.
                * TO: Playing State, when the player unpauses the game.
                * TO: Disoriented State, if the mobile device is in portrait mode.
                */
                UIManager.prototype.startPausedState = function () {
                    //Current score text field
                    this._txtCurrentScore.visible = true;
                    //High score text field
                    this._txtHighScore.visible = true;
                    //Start prompt text field
                    this._txtStartPrompt.visible = true;
                    //Orientation prompt text field
                    this._txtOrientationPrompt.visible = false;
                    //Pause button
                    this._pauseButton.addEventListener("click", this.HandleUserClickedPauseButton);
                    this._pauseButton.visible = true;
                    //Mute button
                    this._muteButton.addEventListener("click", this.HandleUserClickedMuteButton);
                    this._muteButton.visible = true;
                    //Orientation changes
                    window.addEventListener("orientationchange", this.HandleUserChangedOrientation);
                    //PC controls
                    this._worldContainer.removeEventListener("mousedown", this.HandleUserMousedDownWorld);
                    this._worldContainer.removeEventListener("pressup", this.HandleUserMousedUpWorld);
                    //World pause toggle
                    this._worldContainer.addEventListener("click", this.HandleUserClickedWorld);
                };
                /**
                * Starts the disoriented state. In this state, the player has no input options except to turn the mobile device to landscape mode.
                *
                * FROM: Paused State, if the mobile device is in portrait mode.
                * TO: Paused State, when the player turns the mobile device to landscape mode.
                */
                UIManager.prototype.startDisorientedState = function () {
                    //Current score text field
                    this._txtCurrentScore.visible = false;
                    //High score text field
                    this._txtHighScore.visible = false;
                    //Start prompt text field
                    this._txtStartPrompt.visible = false;
                    //Orientation prompt text field
                    this._txtOrientationPrompt.rotation = window.orientation;
                    this._txtOrientationPrompt.visible = true;
                    //Pause button
                    this._pauseButton.visible = false;
                    //Mute button
                    this._muteButton.visible = false;
                    //World pause toggle
                    this._worldContainer.removeEventListener("click", this.HandleUserClickedWorld);
                };
                /**
                * Starts the playing state. In this state, the player controls the PC and plays the game.
                *
                * FROM: Paused State, when the player unpauses the game.
                * TO: Paused State, when the player pauses the game or turns the mobile device to portrait mode.
                * TO: Game Over State, when the player loses the game.
                */
                UIManager.prototype.startPlayingState = function () {
                    //High score text field
                    this._txtHighScore.visible = false;
                    //Start prompt text field
                    this._txtStartPrompt.visible = false;
                    //PC controls
                    this._worldContainer.addEventListener("mousedown", this.HandleUserMousedDownWorld);
                    this._worldContainer.addEventListener("pressup", this.HandleUserMousedUpWorld);
                    //World pause toggle
                    this._worldContainer.removeEventListener("click", this.HandleUserClickedWorld);
                };
                /**
                * Starts the game over state. In this state, the player has no input options until the death animation is complete, after which the game will enter the reset state.
                *
                * FROM: Playing state, when the player loses the game.
                * TO: Reset State, when the PC death animation is complete.
                */
                UIManager.prototype.startGameOverState = function () {
                    //Pause button
                    this._pauseButton.removeEventListener("click", this.HandleUserClickedPauseButton);
                    //Mute button
                    this._muteButton.removeEventListener("click", this.HandleUserClickedMuteButton);
                    //Orientation changes
                    window.removeEventListener("orientationchange", this.HandleUserChangedOrientation);
                    //PC controls
                    this._worldContainer.removeEventListener("mousedown", this.HandleUserMousedDownWorld);
                    this._worldContainer.removeEventListener("pressup", this.HandleUserMousedUpWorld);
                };
                Object.defineProperty(UIManager.prototype, "guiContainer", {
                    //=================================
                    //		ACCESSORS
                    //=================================
                    get: function () {
                        return this._guiContainer;
                    },
                    enumerable: true,
                    configurable: true
                });
                return UIManager;
            })();
            grandPrixGranny.UIManager = UIManager;
        })(grandPrixGranny = jestermen.grandPrixGranny || (jestermen.grandPrixGranny = {}));
    })(jestermen = com.jestermen || (com.jestermen = {}));
})(com || (com = {}));
//# sourceMappingURL=UIManager.js.map